The breakdown of detections in the entire dataset.
Each point on the curve shows how many files (y axis) are labeled as malicious 
by a certain number of AV products (x axis).
